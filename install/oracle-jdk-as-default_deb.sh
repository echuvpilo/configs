#!/bin/sh

update-alternatives --install "/usr/bin/java" "java" "$1/bin/java" 1
update-alternatives --install "/usr/bin/javac" "javac" "$1/bin/javac" 1
update-alternatives --install "/usr/bin/javaws" "javaws" "$1/bin/javaws" 1
update-alternatives --install "/usr/bin/keytool" "keytool" "$1/bin/keytool" 1

update-alternatives --config java
update-alternatives --config javac
update-alternatives --config javaws
update-alternatives --config keytool

