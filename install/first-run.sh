#!/bin/bash

PWD="$(pwd)"
CONFIGS_HOME="$(dirname $PWD)"

DOT_FILES="$CONFIGS_HOME/dot-files"
BIN="$CONFIGS_HOME/bin"

BASH_DOTS="$DOT_FILES/bash"

ln -s $BASH_DOTS/bashrc $HOME/.bashrc
ln -s $BASH_DOTS/bash_profile $HOME/.bash_profile
ln -s $BASH_DOTS/bash_aliases $HOME/.bash_aliases
ln -s $BASH_DOTS/bash_env_vars $HOME/.bash_env_vars
ln -s $DOT_FILES/tmux.conf $HOME/.tmux.conf
ln -s $BIN $HOME/Bin
 
