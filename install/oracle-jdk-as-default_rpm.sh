#!/bin/bash

## java ##
alternatives --install /usr/bin/java java $1/jre/bin/java 200000
## javaws ##
alternatives --install /usr/bin/javaws javaws $1/jre/bin/javaws 200000
 
## Java Browser (Mozilla) Plugin 32-bit ##
alternatives --install /usr/lib/mozilla/plugins/libjavaplugin.so libjavaplugin.so $1/jre/lib/i386/libnpjp2.so 200000
 
## Java Browser (Mozilla) Plugin 64-bit ##
alternatives --install /usr/lib64/mozilla/plugins/libjavaplugin.so libjavaplugin.so.x86_64 $1/lib/amd64/libnpjp2.so 200000
 
## Install javac only if you installed JDK (Java Development Kit) package ##
alternatives --install /usr/bin/javac javac $1/bin/javac 200000
alternatives --install /usr/bin/jar jar $1/bin/jar 200000

## java ##
alternatives --config java 
## javaws ##
alternatives --config javaws 
 
## Java Browser (Mozilla) Plugin 32-bit ##
alternatives --config libjavaplugin.so 
 
## Java Browser (Mozilla) Plugin 64-bit ##
alternatives  libjavaplugin.so.x86_64 
 
## Install javac only if you installed JDK (Java Development Kit) package ##
alternatives --config javac 
alternatives --config jar 

