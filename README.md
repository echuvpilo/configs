# My dot-files and some configuration scripts

* bin
* dot-files
* install

## bin
Here is utility scripts for run some services, tools and other stuff

## dot-files

Configuration files for my *bash*, *vim* and other

## install

Scripts for run after fresh system installed
