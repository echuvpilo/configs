#!/bin/bash

MONGO_BIN_PATH="$MONGO_APP_MAIN/$MONGODB_VERSION/bin"
MONGO_DATA_PATH="$MONGO_DATA_MAIN/$MONGODB_VERSION"

if [ ! -d $MONGO_DATA_PATH ]; then
	mkdir -p $MONGO_DATA_PATH
fi
echo $MONGO_DATA_PATH
$MONGO_BIN_PATH/mongod --httpinterface --jsonp --rest --journal --directoryperdb --dbpath $MONGO_DATA_PATH
